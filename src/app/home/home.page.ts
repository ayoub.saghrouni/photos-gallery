import { Component, OnInit, ViewChild } from '@angular/core';
import { PhotoService } from '../services/photo.service';
import { IonSlides } from '@ionic/angular';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  @ViewChild('slideWithNav', { static: false }) slideWithNav: IonSlides;
  slideOpts = {
    initialSlide: 1,
    slidesPerView: 3,
    speed: 400
  };

  photos: any;
  constructor(public photoService: PhotoService)  { }
  ngOnInit(): void {
    this.photoService.createDB().then(() => {
      this.photoService.createTable().then(() => {
        this.getAllPhotos()
      }).catch(error => console.log(error))
    }).catch(error => console.log(error))


  }

  // add photo from service
  addPhotoToGallery() {
    this.photoService.addPhotoToGallery().then(() => {
      this.getAllPhotos();
    })
  }

  // get all photos from service
  getAllPhotos() {
    this.photoService.getAllPhotos().then(result => {
      console.log(JSON.stringify(result))
      this.photos = result;
    }).catch(error => {
      console.log(error)
    })
  }

  // display photo full screen
  viewPhoto(photo) {
    //let path = photo.webviewPath
    var filename = photo.webviewPath.substring(photo.webviewPath.lastIndexOf('/')+1);
    var path =  photo.webviewPath.substring(0,photo.webviewPath.lastIndexOf('/')+1);
   //then use it in the readAsDataURL method of cordova file plugin
   //this.file is declared in constructor file :File
        // this.file.readAsDataURL(path, filename).then(url=>{
        //   this.photoViewer.show(url, photo.filepath, { share: false });
        // });
    console.log('file path is :', path )
     
  
  
  //  this.fullScreenImage.showImageURL(photo.webviewPath)
  // .then((data: any) => console.log('image dispaly fullscreen'))
  // .catch((error: any) => console.error(error));
  }

}
