import { Injectable } from '@angular/core';
import {
  Plugins, CameraResultType, Capacitor, FilesystemDirectory,
  CameraSource
} from '@capacitor/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Platform } from '@ionic/angular';

const { Camera, Filesystem, Storage } = Plugins;
@Injectable({
  providedIn: 'root'
})

export class PhotoService {
  public photos: Photo[] = [];
  databaseObj: SQLiteObject;
  table_name = "Photo";
  constructor(private sqlite: SQLite, private platform: Platform) { }

  // Create DB if not exist
  createDB() {
    return new Promise((resolve, reject) => {
      this.sqlite.create({
        name: "photos.db",
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.databaseObj = db;
          console.log(' photos Database Created!');
          resolve(true)
        })
        .catch(error => {
          console.log("error " + JSON.stringify(error))
          reject(error)
        });
    });
  }

  // Create table if not exist
  createTable() {
    return new Promise((resolve, reject) => {
      this.databaseObj.executeSql(`CREATE TABLE IF NOT EXISTS ${this.table_name}  (pid INTEGER PRIMARY KEY, filepath varchar(255), webviewPath varchar(255))`, [])
        .then(() => {
          console.log('Table Created!');
          resolve(true);
        })
        .catch(error => {
          console.log("error " + JSON.stringify(error))
          reject(error)
        });
    });
  }

  // add photo to gallery
  public async addPhotoToGallery() {
    // Take a photo
    const capturedPhoto = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality: 100
    });

    let filepath = 'photo' + this.photos.length.toString()
    this.insertPhoto(filepath, capturedPhoto.webPath)
  }
  // Insert photo in the database
  insertPhoto(filepath, webviewPath) {
    this.databaseObj.executeSql(`INSERT INTO ${this.table_name} (filepath , webviewPath) VALUES ('${filepath}', '${webviewPath}' )`, [])
      .then(() => {
        console.log('Row Inserted!');
      })
      .catch(error => {
        alert("error " + JSON.stringify(error))
      });
  }
  // Retrieve all photos 
  getAllPhotos() {
    return new Promise((resolve, reject) => {
      this.databaseObj.executeSql(`
    SELECT * FROM ${this.table_name}`, [])
        .then((res) => {
          this.photos = [];
          if (res.rows.length > 0) {
            for (var i = 0; i < res.rows.length; i++) {
              this.photos.push(res.rows.item(i));
            }
            console.log(this.photos)
            resolve(this.photos)
          }
        })
        .catch(error => {
          reject(error);
          console.log("error " + JSON.stringify(error))
        });
    });
  }
}
interface Photo {
  pid?: number;
  filepath: string;
  webviewPath: string;
}